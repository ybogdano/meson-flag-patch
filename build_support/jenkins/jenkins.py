# Copyright (C) Intel Corp.  2014-2020.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/

from urllib.request import urlopen, quote
import ast
import json
import os
import signal
import sys
import tempfile
import time
import xml.sax.saxutils
import subprocess
import git

if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.abspath(sys.argv[0])))

from export.export_build import BuildUploadQueue, upload_build_status

from project_invoke import ProjectInvoke
from dependency_graph import DependencyGraph
from project_map import ProjectMap
from repo_set import RepoSet
from utils.utils import reliable_url_open
from utils.command import run_batch_command
from export import Export

ABORT_RESULT_PATH = ""
ABORT_MASTER_HOST = ""
ABORT_MAIN_BUILD_INFO_ARGS = {}

def abort_builds(*_):
    """Signal handler for build cancellation.  Invokes a jenkins build
    to cancel all components for the current build.  Matching builds
    are identified based on the result path, which may never collide.
    """
    global ABORT_RESULT_PATH
    if not ABORT_RESULT_PATH:
        # only abort once
        return
    print(f"Cancelling builds at {ABORT_RESULT_PATH}")
    url = (f"http://{ABORT_MASTER_HOST}/job/public/job/cancel_components/"
           f"buildWithParameters?result_path={ABORT_RESULT_PATH}")
    try:
        reliable_url_open(url, method="POST").read()
    except Exception:
        pass
    # only abort once
    ABORT_RESULT_PATH = ""

    ABORT_MAIN_BUILD_INFO_ARGS["main_build_info"]["status"] = "completed"
    ABORT_MAIN_BUILD_INFO_ARGS["main_build_info"]["end_time"] = time.time()
    try:
        if ABORT_MAIN_BUILD_INFO_ARGS["export"]:
            upload_build_status(
                ABORT_MAIN_BUILD_INFO_ARGS["main_build_info"],
                ABORT_MAIN_BUILD_INFO_ARGS["public_result_hosts"],
                ABORT_MAIN_BUILD_INFO_ARGS["private_result_hosts"],
                ABORT_MAIN_BUILD_INFO_ARGS["internal"]
            )
        Jenkins.write_main_build_info(
            ABORT_MAIN_BUILD_INFO_ARGS["main_build_info"]
        )
    except Exception:
        pass
    print("waiting for cancellation to finish")
    time.sleep(30)
    raise BuildAborted()

class AlreadyBuilt(Exception):
    def __init__(self, invoke):
        Exception.__init__(self)
        self.invoke = invoke

    def __str__(self):
        return "Already Built: " + str(self.invoke)

class BuildInProgress(Exception):
    def __init__(self, invoke):
        Exception.__init__(self)
        self.invoke = invoke
    def __str__(self):
        return "Error: build in progress.  Remove the following file to " \
            "rebuild: " + self.invoke.info_file()

class BuildFailure(Exception):
    def __init__(self, invoke, url):
        Exception.__init__(self)
        self.invoke = invoke
        self.url = url

    def __str__(self):
        return "Error: build failure: " + str(self.invoke) + "\nError: " + \
            self.url

class BuildAborted(Exception):
    def __init__(self):
        Exception.__init__(self)

    def __str__(self):
        return "Error: build aborted from jenkins"

class BuildStatus():
    def __init__(self, invoke, url, status):
        self.invoke = invoke
        self.url = url
        self.status = status

class Jenkins:
    """Manages invocation and status of builds on CI server
    """
    status_colors = {
                'success':'#66FF33',    # light green
                'failure':'#CD5555',     # light red
                'aborted':'#FFA54F',  # light orange
                'prebuilt':'#FFFFFF',   # white
                'unstable':'#F7FE2E',   # yellow
                'unknown':'#EE82EE'}    # purple

    def __init__(self, revspec, result_path, internal=False, export=True):
        if "http_proxy" in os.environ:
            del os.environ["http_proxy"]

        project_map = ProjectMap()
        self._server = project_map.build_spec().master_host()

        # list of buildinvoke objects that have been triggered
        self._jobs = []
        self._completed_builds = []
        self._revspec = revspec
        self._repo_set = RepoSet(project_map=project_map)
        self._result_path = result_path
        self._internal = internal
        self._public_result_hosts = []
        if not internal:
            self._public_result_hosts += ["mesa-ci.01.org"]
        self._private_result_hosts = ["mesa-ci-results.local"]
        # should results be exported to the results sites?
        self._export = export
        self._time = str(time.time())
        self._exporter = Export(project_map)
        self._main_build_info = {"job": os.getenv("JOB_BASE_NAME"),
                                 "build": os.getenv("BUILD_NUMBER"),
                                 "name": os.getenv("BUILD_DISPLAY_NAME"),
                                 "url": os.getenv("BUILD_URL"),
                                 "result_path": self._result_path,
                                 "revisions": (self._repo_set.
                                               serializable_details())
                                 }

        global ABORT_RESULT_PATH, ABORT_MASTER_HOST
        ABORT_RESULT_PATH = result_path
        ABORT_MASTER_HOST = self._server

    def job_url(self, invoke):
        if "win" in invoke.options.hardware:
            return "http://" + self._server + "/job/Winconformance"
        return "http://" + self._server + "/job/public/job/conformance"

    def builds_in_queue(self, priority=None):
        """ Get the number of builds in the Jenkins queue, if priority is
        specified then get number of builds in the queue with the given
        priority """
        resp = reliable_url_open('http://' + self._server
                                 + '/queue/api/json').read()
        queued_builds = json.loads(resp)['items']

        num_builds = 0
        for build in queued_builds:
            params = {p[0]: p[1] for p in [s.split('=')
                                           for s in build['params'].split('\n')
                                           if s]}
            if 'ExecutorStepExecution$PlaceholderTask' in build['task']['_class']:
                continue
            if build['task']['name'] == 'reboot_single':
                continue
            if 'clean-workspace' in build['params']:
                continue
            if not priority:
                num_builds += 1
            elif priority and params.get('BuildPriority') == str(priority):
                num_builds += 1
            # else don't count the build, since it doesn't match the priority

        return num_builds

    def write_failure_log(self, project_invoke):
        job_url = project_invoke.get_info("url")
        log_file_name = project_invoke.to_short_string().replace(" ", "_") + ".log"
        with tempfile.TemporaryDirectory() as tempd:
            run_batch_command(["wget", "-o", "/dev/null", "-O", f"{tempd}/{log_file_name}",
                               job_url + "/consoleText"], quiet=True)
            self._exporter.copy(f"{tempd}/{log_file_name}", project_invoke.component_result_dir())

    def build(self, project_invoke, uploadq, branch="", extra_arg=None, priority=3):
        status = project_invoke.get_info("status", block=False)
        if status == "building" or status == "queued":
            job_url = project_invoke.get_info("url", block=False)
            live_build = True
            if not job_url:
                print("No URL, checking trigger time")
                trigger_time = project_invoke.get_info("trigger_time")
                if trigger_time:
                    trigger_time = float(trigger_time)
                    if time.time() - trigger_time > 600:
                        # a build was cancelled in the queue
                        print("build was triggered but never built: " + str(trigger_time))
                        live_build = False
            try:
                f = urlopen(job_url + "/api/python")
                abuild_page = ast.literal_eval(f.read().decode("utf-8"))
                if abuild_page["result"]:
                    # there is a build result, probably cancelled
                    print("found dead build with status: " + abuild_page["result"])
                    print("url: " + job_url)
                    print("rebuilding.")
                    live_build = False
            except:
                pass
            if live_build:
                print(f"WARN: rebuilding queued job: {project_invoke.info_file()}")
            # else the build is dead, we should rebuild it.

        if status == "failure":
            # raise BuildFailure(project_invoke, self._revision)
            # for now, let's attempt to rebuild failure projects
            pass

        project_invoke.set_info("status", "queued")
        project_invoke.set_info("url", "")
        project_invoke.url = "enqueued"
        self._jobs.append(project_invoke)
        if self._export:
            uploadq.upload_component(self._main_build_info,
                                     project_invoke,
                                     self._public_result_hosts,
                                     self._private_result_hosts,
                                     internal=self._internal)

        # use the current build_support branch on the component builds
        r = git.Repo(ProjectMap().source_root())
        bs_branch = r.commit().hexsha
        url = ("{0}/buildWithParameters?{1}&branch={2}"
               "&build_support_branch={3}&BuildPriority={4}").format(
                   self.job_url(project_invoke),
                   self._jenkins_params(project_invoke),
                   branch,
                   bs_branch,
                   priority
               )
        if extra_arg:
            url = url + "&extra_arg=" + quote(extra_arg)

        f = reliable_url_open(url, method="POST")
        f.read()
        return True

    def reboot_builder(self, builder):
        self._job_url = "http://" + self._server + "/job/conformance"
        url = "http://{0}/job/public/job/reboot_single/buildWithParameters?label={1}".format(
            self._server,
            builder)

        f = reliable_url_open(url, method="POST")
        f.read()
        return True

    def _jenkins_params(self, invoke):
        p = []
        o = invoke.options
        p.append("project=" + invoke.project)
        p.append("arch=" + o.arch)
        p.append("config=" + o.config)
        p.append("type=" + o.type)
        p.append("revision=" + \
                 quote(invoke.revision_spec.to_cmd_line_param()))
        p.append("result_path=" + o.result_path)
        p.append("hardware=" + o.hardware)
        p.append("hash=" + invoke.hash(self._time))
        p.append("shard=" + o.shard)
        if o.retest_path:
            p.append("extra_arg=--retest_path=" + o.retest_path)
        if o.env:
            p.append("env=" + quote(o.env))

        label = o.hardware
        label = label.removesuffix("_zink")
        p.append("label=" + label)

        return "&".join(p)

    def get_completed_builds(self):
        return self._completed_builds

    def print_builds(self):
        if not self._jobs:
            return
        print("The following builds are executing on the build system:")
        for a_job in self._jobs:
            print(f"\t{a_job.to_short_string()} : {a_job.url}")

    def wait_for_build(self, quick=False):
        if not self._jobs:
            return None

        # iterate _jobs, searching for matching job on jenkins.
        # Return any job that is complete.  Raise error if matching
        # job failure.
        first_round = True
        while True:
            # moderate the rate of polling, but do not add any latency
            # to the first attempt to find a finished build.
            if not first_round:
                time.sleep(5)
            first_round = False

            # find urls for new builds
            self.get_matching_builds()

            for i in range(len(self._jobs)):
                a_job = self._jobs[i]
                # get the url from the build_info file, if possible
                job_url = a_job.url
                if job_url == "enqueued":
                    continue

                try:
                    f = urlopen(job_url + "/api/python")
                except:
                    continue
                abuild_page = ast.literal_eval(f.read().decode("utf-8"))

                if not abuild_page["result"]:
                    # build not complete yet
                    continue

                end_time = a_job.get_info("end_time")

                if ((not end_time or (time.time() - end_time < 10))
                        and not quick):
                    # build will temporarily report success until
                    # warnings and test results are parsed.  This
                    # takes just a few seconds.  Re-read the status
                    # page to ensure we have the final status.  Only
                    # incur delays if the build finished less than 10
                    # seconds ago.
                    time.sleep(10 )
                try:
                    f = urlopen(job_url + "/api/python")
                except:
                    continue

                end_time = a_job.get_info("end_time")
                abuild_page = ast.literal_eval(f.read().decode("utf-8"))

                a_job.set_info("collect_time", time.time())

                if not end_time:
                    a_job.set_info("end_time", float(abuild_page["timestamp"] + abuild_page["duration"]) / 1000)
                if (abuild_page["result"] == "SUCCESS" or 
                    abuild_page["result"] == "UNSTABLE"):

                    return BuildStatus(self._jobs.pop(i), 
                                       abuild_page["url"], 
                                       abuild_page["result"].lower())
                self._jobs.pop(i)
                raise BuildFailure(a_job, abuild_page["url"])
            if quick:
                break

    def get_matching_builds(self):
        build_pages = {}
        job_page = None
        max_job = None
        for a_job in self._jobs:
            if a_job.url != "enqueued":
                continue
            if not job_page:
                job_url = self.job_url(a_job)
                f = None
                try:
                    f = urlopen(job_url + "/api/python")
                except:
                    return
                job_page = ast.literal_eval(f.read().decode("utf-8"))
                max_job = job_page["nextBuildNumber"]
            # check last 400 builds
            for abuild_number in range(max_job-1, max_job - 401, -1):
                build_page = None
                if abuild_number in build_pages:
                    build_page = build_pages[abuild_number]
                else:
                    try:
                        f = urlopen(job_url + "/" + 
                                            str(abuild_number) + "/api/python")
                    except:
                        continue
                    build_page = ast.literal_eval(f.read().decode("utf-8"))
                    build_pages[abuild_number] = build_page
                if not self.match_project_invoke(a_job, build_page):
                    # not the matching build for the job that we are looking for
                    continue
                url = build_page["url"]
                build_num = build_page["number"]
                a_job.set_info("url", url)
                a_job.url = url
                a_job.set_info("build", build_num)

                with tempfile.NamedTemporaryFile(mode="w") as tmpf:
                    info_fpath = os.path.join(self._result_path,
                                              "component_info_paths",
                                              a_job.hash(
                                                  os.path.basename(
                                                      os.path.normpath(self._result_path))))

                    print(a_job.info_dir(), file=tmpf, flush=True)

                    try:
                        self._exporter.copy(tmpf.name, info_fpath)
                    except subprocess.CalledProcessError as e:
                        print("WARN: error writing info_file path: " + str(e))

                print(url + " found for " + a_job.to_short_string())
                break

    def match_project_invoke(self, project_invoke, build_dict):
        """true if the dict matches the invoke"""

        # get build_params
        build_params = []
        for an_action in build_dict["actions"]:
            if "parameters" in an_action:
                build_params = an_action["parameters"]
                break
        if not build_params:
            print("WARN: build without params when searching for: " + project_invoke.to_short_string())
            return False
        hash_str = ""
        for a_param in build_params:
            if a_param["name"] == "hash":
                hash_str = a_param["value"]
                break
        return hash_str == project_invoke.hash(self._time)

    def get_build_link(self, project_invoke, block=True):
        while True:
            self.get_matching_builds()
            # first check to see if the url is already known
            url = project_invoke.get_info("url")
            if url:
                return url
            if not block:
                return None
            time.sleep(1)

    def build_all(self, depGraph, branch="mesa_master", print_summary=True,
                  priority=3):
        signal.signal(signal.SIGINT, abort_builds)
        signal.signal(signal.SIGABRT, abort_builds)
        signal.signal(signal.SIGTERM, abort_builds)

        all_sources = depGraph.all_sources()
        triggered_builds = []
        ready_for_build = depGraph.ready_builds()
        assert ready_for_build
        failure_builds = []
        pm = ProjectMap()
        summary_xml = pm.source_root() + "/summary.xml"
        if os.path.exists(summary_xml):
            os.remove(summary_xml)
        self._main_build_info.update({
            "status": "initializing",
            "start_time": time.time(),
            "end_time": time.time()
        })
        success = True
        with BuildUploadQueue() as uploadq:
            if self._export:
                uploadq.upload_build_status(
                    self._main_build_info,
                    self._public_result_hosts,
                    self._private_result_hosts,
                    internal=self._internal
                )

            self._write_main_build_info()

            self._main_build_info["status"] = "building"
            global ABORT_MAIN_BUILD_INFO_ARGS
            ABORT_MAIN_BUILD_INFO_ARGS = {
                "private_result_hosts": self._private_result_hosts,
                "public_result_hosts": self._public_result_hosts,
                "main_build_info": dict(self._main_build_info),
                "internal": self._internal,
                "export": self._export
            }
            while success:
                if len(self._jobs) < 10:
                    self.print_builds()
                else:
                    job_interval = int(len(self._jobs) / 10)
                    if len(self._jobs) % job_interval == 0:
                        self.print_builds()
                builds_in_round = 0
                for an_invoke in ready_for_build:
                    status = an_invoke.get_info("status", block=False)

                    an_invoke.set_info("name", an_invoke.project)
                    if status == "success" or status == "unstable":
                        # don't rebuild if we have a good build, or just
                        # because some tests failure
                        self._completed_builds.append(an_invoke)
                        depGraph.build_complete(an_invoke)
                        if self._export:
                            uploadq.upload_component(self._main_build_info,
                                                     an_invoke,
                                                     self._public_result_hosts,
                                                     self._private_result_hosts,
                                                     internal=self._internal)
                        builds_in_round += 1
                        print("Already built: " + an_invoke.to_short_string())
                        continue
                    elif status == "queued":
                        try:
                            # raises a KeyError if job
                            # hasn't landed on a machine yet
                            an_invoke.get_info("machine", block=False)

                            an_invoke.set_info("status", "building")
                            if self._export:
                                uploadq.upload_component(self._main_build_info,
                                                         an_invoke,
                                                         self._public_result_hosts,
                                                         self._private_result_hosts,
                                                         internal=self._internal)
                        except KeyError:
                            pass

                    proj_build_dir = pm.project_build_dir(an_invoke.project)
                    script = proj_build_dir + "/build.py"
                    if not os.path.exists(script):
                        depGraph.build_complete(an_invoke)
                        builds_in_round += 1
                        continue
                    # number of seconds to wait before
                    # triggering a build if queue is full
                    queue_wait_sec = 5
                    try:
                        # Limit the number of builds triggered
                        # in jenkins, 20 for each job priority
                        while self.builds_in_queue(priority=priority) >= 40:
                            print("Too many jobs in queue, trying again in "
                                  "{} seconds".format(queue_wait_sec))
                            time.sleep(queue_wait_sec)
                            builds_in_round += self.process_finished_build(
                                depGraph,
                                uploadq,
                                failure_builds,
                                quick=True
                            )
                        print("Starting: " + an_invoke.to_short_string())
                        self.build(an_invoke,
                                   uploadq,
                                   branch=branch,
                                   priority=priority)
                        an_invoke.set_info("trigger_time", time.time())
                        triggered_builds.append(an_invoke)

                    except BuildInProgress as e:
                        print(e)
                        success = False
                        break

                if not success:
                    break

                if builds_in_round:
                    ready_for_build = depGraph.ready_builds(triggered_builds)
                    continue
                builds_in_round += self.process_finished_build(depGraph,
                                                               uploadq,
                                                               failure_builds)

                if not builds_in_round:
                    # nothing was built, there was no failure, and no
                    # builds are ready => the last project is built

                    # stub_test_results(out_test_dir, o.hardware)
                    # CleanServer(o).clean()
                    if print_summary:
                        write_summary(pm.source_root(),
                                      failure_builds + self._completed_builds,
                                      self, all_sources)

                    try:
                        if failure_builds:
                            raise BuildFailure(failure_builds[0], "")
                    finally:
                        self._main_build_info["status"] = "completed"
                        self._main_build_info["end_time"] = time.time()
                        if self._export:
                            uploadq.upload_build_status(
                                self._main_build_info,
                                self._public_result_hosts,
                                self._private_result_hosts,
                                internal=self._internal
                            )

                        self._write_main_build_info()

                    return triggered_builds

                ready_for_build = depGraph.ready_builds(triggered_builds)

            self._main_build_info["status"] = "completed"
            self._main_build_info["end_time"] = time.time()
            if self._export:
                uploadq.upload_build_status(
                    self._main_build_info,
                    self._public_result_hosts,
                    self._private_result_hosts,
                    internal=self._internal
                )

            self._write_main_build_info()

            return triggered_builds

    def process_finished_build(
            self,
            depGraph,
            uploadq,
            failure_builds,
            quick=False
    ):
        """ scan for and processes completed build. Returns the number of
        builds that have completed """
        finished = None
        builds = 0
        retry = False
        try:
            finished = self.wait_for_build(quick=quick)
            if finished:
                builds = 1
                if ("buildtest" in finished.invoke.project
                        and finished.status == "unstable"):
                    self.write_failure_log(finished.invoke)
        except BuildFailure as failure:
            finished = BuildStatus(
                failure.invoke,
                failure.url,
                "failure"
            )
            invoke = failure.invoke
            invoke.set_info("status", "failure")
            print(f"Build failure: {failure.url}")
            print(f"Build failure: {invoke.to_short_string()}")
            self.write_failure_log(invoke)
            if (
                invoke.options.hardware in ['xe_dg2', 'lnl', 'bmg'] and
                invoke not in failure_builds
            ):
                finished = None
                retry = True
                print("Retrying build: " + failure.url)
                self.build(invoke,
                           uploadq,
                           branch="unstable_rebuild",
                           priority=2)
                print("sleeping to allow retry to enqueue")
                time.sleep(30)
            failure_builds.append(invoke)
            if not retry:
                depGraph.build_failed(invoke)
            builds = 1

        if retry:
            builds = self.process_finished_build(
                depGraph,
                uploadq,
                failure_builds,
                quick=quick
            )

        if finished:
            finished.invoke.set_info("status", finished.status)
            print("Build finished: " + finished.invoke.to_short_string()
                  + " " + finished.url)

            self._completed_builds.append(finished.invoke)
            depGraph.build_complete(finished.invoke)

            self._main_build_info["end_time"] = time.time()
            if self._export:
                print("Uploading build to results server...")
                uploadq.upload_component(
                    self._main_build_info,
                    finished.invoke,
                    self._public_result_hosts,
                    self._private_result_hosts,
                    internal=self._internal
                )

        return builds

    @staticmethod
    def write_main_build_info(main_build_info, exporter=None):
        """Write full build stats to the results dir"""
        main_build_info = dict(main_build_info)
        remote_path = os.path.join(
            main_build_info["result_path"],
            "build_info.json"
        )
        with tempfile.NamedTemporaryFile(mode="w") as tmpf:
            exporter = exporter or Export()

            json.dump(main_build_info, tmpf)
            tmpf.flush()
            exporter.copy(tmpf.name, remote_path)

    def _write_main_build_info(self):
        """Write full build stats to the results dir"""
        Jenkins.write_main_build_info(self._main_build_info,
                                      exporter=self._exporter)


def generate_color_key(ljen):
    out_key = r'<field name="Key" titlecolor="black" value="" ' \
              r'detailcolor="" href="" /><table><tr>'

    for status in sorted(ljen.status_colors):
        out_key += '<td value="" bgcolor="' + ljen.status_colors[status] + \
                   '" fontcolor="black" fontattribute="normal" ' \
                   'align="center" width="200"/>'

    out_key += r'</tr>'
    out_key += r'<tr>'
    for status in sorted(ljen.status_colors):
        out_key += '<td value="' + status + \
                   '" bgcolor="" fontcolor="black" fontattribute="normal" '\
                   'align="center" width="200"/>'

    out_key += r'</tr></table>'
    return out_key


def generate_summary_row(build, ljen, header=False):
    if header:
        options_dict = {}
        bg_color = '#C6E2FF'
        font_attr = 'bold'
        link = ''
    else:
        font_attr = 'normal'
        bg_color = ljen.status_colors.get(build.get_info("status"), 
                                          ljen.status_colors['unknown'])
        options_dict = vars(build.options)
        link = ljen.get_build_link(build, block=False)
        options_dict['project'] = build.project
        #options_dict['platform'] = build.platform
        duration = hours_minutes_seconds(build.get_info("collect_time"), 
                                         build.get_info("trigger_time"))
        options_dict['duration'] = duration
                                   
        if not link:
            bg_color = ljen.status_colors['prebuilt']  
    out_row = r'<tr>'
    for afield in ['project', 'arch', 'hardware',
                   'type', 'config', 'duration']:
        add_url = ''
        if afield == 'project' and link:
            add_url = 'href="' + link + '"'

        out_row += '<td value="' + options_dict.get(afield, afield) + \
                   '" bgcolor="' + bg_color + \
                   '" fontcolor="black" fontattribute="' + \
                   font_attr + '" align="center" ' + add_url + \
                   ' width="200"/>'

    out_row += r'</tr>'
    return out_row


def hours_minutes_seconds(finish_time, start_time):
    if not finish_time or not start_time:
        return ""
    duration = finish_time - start_time
    seconds = int(duration % 60)
    minutes = int((duration / 60) % 60)
    hours = int(duration / 3600)
    return "%02d:%02d:%02d" % (hours, minutes, seconds)


def refresh_status(build):
    build_page = None
    info = build._read_info()
    if "url" not in info:
        return
    url = info["url"]
    for _ in range(0,10):
        try:
            f = urlopen(f"{url}/api/python")
            build_page = ast.literal_eval(f.read().decode('utf-8'))
            break
        except:
            print(f"Retrying read of build page: {url}")
            time.sleep(1)
            continue

    if not build_page:
        return
    if not build_page["result"]:
        return
    if "status" in info:
        if info["status"] == build_page["result"].lower():
            return
    build.set_info("status",  build_page["result"].lower())


def write_summary(out_dir, completed_builds, ljen, project_sources):
    if completed_builds and type(completed_builds[0]) == type(""):
        # we have been passed a list of invoke strings instead of
        # objects.  Convert them
        invoke_builds = [ProjectInvoke(from_string=buildstr) for buildstr in completed_builds]
        completed_builds = invoke_builds
    repo_set = RepoSet()

    git_details = {}
    for project, rev in ljen._revspec._revisions.items():
        if project not in project_sources:
            continue
        message = repo_set.repo(project).commit().message.splitlines()[0]
        author = repo_set.repo(project).commit().author.name
        details = {}
        # allow for special chars in messages.
        details["message"] = xml.sax.saxutils.quoteattr(message)
        details["author"] = xml.sax.saxutils.quoteattr(author)
        details["revision"] = rev
        git_details[project] = details
    rev_list = []
    # rev_list is used for creating the retest build url in the summary
    rev_list = ['{}={}'.format(proj, details["revision"])
                for (proj, details) in git_details.items()]
    outf = open(os.path.join(out_dir, "summary.xml"), "w")
    outf.write("""\
<section name="" fontcolor="">
    <field name="Git revisions"/>
    <table>
        <tr>
            <td value="commit" bgcolor="#C6E2FF" fontcolor="black" fontattribute="bold" align="center"  width="200"/>
            <td value="author" bgcolor="#C6E2FF" fontcolor="black" fontattribute="bold" align="center"  width="200"/>
            <td value="description" bgcolor="#C6E2FF" fontcolor="black" fontattribute="bold" align="center"  width="200"/>
        </tr>""")
    for project, details in git_details.items():
        outf.write("""\
        <tr>
            <td value="{project}={revision}" bgcolor="#66FF33" fontcolor="black" fontattribute="normal" align="center"  width="200"/>
            <td value={author} bgcolor="#66FF33" fontcolor="black" fontattribute="normal" align="center"  width="200"/>
            <td value={log} bgcolor="#66FF33" fontcolor="black" fontattribute="normal" align="center"  width="200"/>
        </tr>""".format(project=project, revision=details["revision"], author=details["author"], log=details["message"]))
    outf.write("""\
    </table>
    <table sorttable="yes">""")
    outf.write(generate_summary_row(None, ljen, header=True))
    for build in completed_builds:
        refresh_status(build)
        outf.write(generate_summary_row(build, ljen))
    outf.write("""\
    </table>
    <br />""")
    outf.write(generate_color_key(ljen))
    long_pole_row_txt = """
<tr>
    <td value="{project}" bgcolor="{color}" fontcolor="black" fontattribute="bold" align="center" width="200" href="{url}"/>
    <td value="{duration}" bgcolor="{color}" fontcolor="black" fontattribute="bold" align="center" width="200"/>
    <td value="{waiting}" bgcolor="{color}" fontcolor="black" fontattribute="bold" align="center" width="200"/>
    <td value="{building}" bgcolor="{color}" fontcolor="black" fontattribute="bold" align="center" width="200"/>
    <td value="{finishing}" bgcolor="{color}" fontcolor="black" fontattribute="bold" align="center" width="200"/>
</tr> """
    outf.write("""\
<br/>
<field name="Long Pole Builds" titlecolor="black" value="" detailcolor="" href="" />
<table sorttable="yes">""" + \
               long_pole_row_txt.format(project="project", 
                                        duration="total duration", 
                                        waiting="waiting for machine", 
                                        building="building", 
                                        finishing="summarizing results", 
                                        color="#C6E2FF",
                                        url=""))

    long_pole_builds = DependencyGraph.long_pole(completed_builds[-1])
    long_pole_builds.reverse()
    job_name=os.environ.get("JOB_NAME")
    if '/' in job_name:
        job_components = job_name.split("/")
        visibility = job_components[0]
        job_base_name = job_components[1]
        job_name = f"{visibility}/job/{job_base_name}"
    for a_build in long_pole_builds:
        link = ljen.get_build_link(a_build, block=False)
        duration=hours_minutes_seconds(a_build.get_info("collect_time"), 
                                       a_build.get_info("trigger_time"))
        waiting = hours_minutes_seconds(a_build.get_info("start_time"), 
                                        a_build.get_info("trigger_time"))
        building = hours_minutes_seconds(a_build.get_info("end_time"), 
                                         a_build.get_info("start_time"))
        finishing = hours_minutes_seconds(a_build.get_info("collect_time"), 
                                          a_build.get_info("end_time"))
        outf.write(long_pole_row_txt.format(project=a_build.project, 
                                            duration=duration, 
                                            waiting=waiting, 
                                            building=building, 
                                            finishing=finishing, 
                                            url=link,
                                            color="#66FF33"))

    outf.write("""\
</table>
<br/>
    <field name="Build link to accept changed tests" 
    titlecolor="black" 
    value="generate patch" 
    detailcolor="" 
    href="http://{server}/job/public/job/accept_failed_tests/parambuild/?result_path={result_path}" />
<br/>
<br/>
    <field name="Build link to bisect changed tests"
    titlecolor="black" 
    value="generate patch" 
    detailcolor="" 
    href="http://{server}/job/public/job/bisect_failures/parambuild/?result_path={result_path}" />
<br/>
<br/>
    <field name="Build link to retest failed components"
    titlecolor="black" 
    value="retest" 
    detailcolor="" 
    href="http://{server}/job/{job_name}/parambuild/?revision={rev_list}" />
<br/>
    <field name="Test Results" titlecolor="black" value="" detailcolor="" href="" />
</section>""".format(result_path=ljen._result_path, server=ljen._server,
                     rev_list='%20'.join(rev_list),
                     job_name=job_name))
    outf.close()
